﻿using System;

namespace ProjectEuler.Solutions
{
    internal class EvenFibonacciNumbers : IEulerSolution
    {
        public void StartThisProgram()
        {
            var sum = EvenFibonacciNumbersSum();

            Console.WriteLine(sum);


        }

        private static double EvenFibonacciNumbersSum()
        {
            double x = 1, y = 2;
            double nextFiboNum = y;
            double sum = 0;

            while (nextFiboNum<4000000)
            {
                if (nextFiboNum % 2 == 0)
                    sum += nextFiboNum;

                nextFiboNum = x + y;
                x = y;
                y = nextFiboNum;
                
            }

            return sum;


        }
    }

    
}
