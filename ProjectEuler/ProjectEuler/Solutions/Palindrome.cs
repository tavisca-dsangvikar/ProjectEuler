﻿using System;

namespace ProjectEuler.Solutions
{
    public class Palindrome : IEulerSolution
    {
        public void StartThisProgram()
        {
            Console.WriteLine(GetPalindrome());
        }

        private int GetPalindrome()
        {
            int x = 100;
            int palindrome = 0;
            int number;

            while (x <= 999)
            {
                int y = 100;
                while (y <= 999)
                {
                    number = x * y;
                    if (ReverseString(number.ToString()).Equals(number.ToString()) && palindrome<number)
                        palindrome = number;

                    y++;
                }
                x++;
            }

            return palindrome;
        }

        private string ReverseString(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    }
}