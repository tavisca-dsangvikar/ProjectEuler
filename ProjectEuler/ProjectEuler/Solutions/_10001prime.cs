﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEuler.Solutions
{
    class _10001prime:IEulerSolution
    {
        public void StartThisProgram()
        {
            double num = 2;
            var list = new List<double>();
            int counter = 0;

            while (true)
            {
                if(list.Any(i => num % i == 0) == false)
                { list.Add(num);
                    counter++;
                }

                try
                {
                    if (counter == 10001)
                        Console.WriteLine(list[10001]);
                }
                catch (Exception)
                {

                    Console.WriteLine(list.LastOrDefault());
                }

                num++;
            }
        }
    }
}
