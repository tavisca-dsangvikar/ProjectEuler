﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEuler.Solutions
{
    class SumSquareDifference:IEulerSolution
    {
        public void StartThisProgram()
        {
            int sumOfSquare = GetSumOfSquare();
            int SquareOfSum = GetSquareOfSum();

            Console.WriteLine(SquareOfSum-sumOfSquare);
        }

        private int GetSquareOfSum()
        {
            int sum = 0;

            for (int i = 0; i < 101; i++)
            {
                sum += i;
            }

            return sum*sum;
        }

        private int GetSumOfSquare()
        {
            int sum = 0;

            for (int i = 1; i <= 100; i++)
            {
                sum += i*i;
            }

            return sum;
        }
    }
}
