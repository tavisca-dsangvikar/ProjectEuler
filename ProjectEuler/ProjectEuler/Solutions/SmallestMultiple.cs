﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEuler.Solutions
{
    class SmallestMultiple:IEulerSolution
    {
        public void StartThisProgram()
        {
            int x = GetSmallestMultiple();

            Console.WriteLine(x);
        }

        private int GetSmallestMultiple()
        {
            int x = 20;
            

            while(true)
            {
                bool flag = GetDivisiblity(x);

                if (flag)
                    return x;

                x++;
            }
        }

        private bool GetDivisiblity(int x)
        {
            int[] array = { 2, 3, 4, 5, 7, 9, 11, 13, 16, 17, 19, 20 };
            foreach (int i in array)
            {
                if (x % i != 0)
                    return false;
            }
            return true;
        }
    }
}
