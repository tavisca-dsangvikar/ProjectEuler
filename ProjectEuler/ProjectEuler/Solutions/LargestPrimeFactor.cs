﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectEuler.Solutions
{
    class LargestPrimeFactor : IEulerSolution
    {
        public void StartThisProgram()
        {
            var largestNum = GetLargestPrime();

            if(largestNum != 0)
                Console.WriteLine(largestNum);


        }

        private double GetLargestPrime()
        {
            var primeList = new List<double>();
            double x = 2;
            double number = 600851475143;


            while(x<number )
            {
                if (number % x == 0 && primeList.Any(i => x % i == 0) == false)
                    primeList.Add(x);
                x++;
            }

            return primeList.LastOrDefault();
        }

        
    }
}
